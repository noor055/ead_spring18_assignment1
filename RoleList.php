<?php
session_start();
require_once 'db.php';
?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
        <title>Role List</title>
    </head>

	<body background="grid.jpg">
        <div class="navbar">
                  <a href="Home.php">Home</a> 
                  <a href="UserList.php">User Management</a>
                  <a href="RoleList.php">Role Management</a>
                  <a href="PermissionsList.php">Permissions Mangement</a> 
                  <a href="RolePermissionList.php">Role Permissions Mangement</a>
                  <a href="UserRolesList.php">User-Role Assignment</a>
                  <a href="History.php">Login History</a>
                  <a href="Logout.php">Logout</a>
                
        </div>


        <table style="padding-top:40px; padding-left: 200px" >
                <tr>
                   <a href="Role.php"><button class="margin-button"><strong>Add New Role</strong></button></a> 
                </tr>
                
		    <?php
            $query = "SELECT roleid,name,description FROM roles";
            $result = mysqli_query($conn, $query);
            $recordsFound = mysqli_num_rows($result);     
            
            if ($recordsFound > 0) {
              
            ?>
                
                <tr >

                  <th class="tablestyle">ID</th>
                  <th class="tablestyle">Name</th>
                  <th class="tablestyle">Description</th>
                  <th class="tablestyle"> Edit</th>
                  <th class="tablestyle">Delete</th>

               </tr>
          
      <?php
        while($row = mysqli_fetch_assoc($result)) {
              
          $id = $row['roleid'];
          $name = $row['name'];
          $description = $row['description']; 
          
          echo "<tr>";

          echo '<td style="padding-right: 50px">' . $id . "</td>";
          echo '<td style="padding-right: 50px">' . $name . "</td>";
          echo "<td>" . $description . "</td>";?>


          <th><a href="Role.php?id=<?php echo $id; ?>">Edit</a></th>
          <th><a href="Role.php?delete&id=<?php echo $id; ?>" onclick="return confirm('Are you really want to delete?')">Delete</a></th>
         <?php
          echo "</tr>";
          

        }

        echo "</table>";
      }       
?>
				

		
		</body>
</html>
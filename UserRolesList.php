<?php
session_start();
require_once 'db.php';
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		 <title>User-Role List</title>



	</head>

	<body background = "grid.jpg" >
           <div class="navbar">
                   <a href="Home.php">Home</a> 
                  <a href="UserList.php">User Management</a>
                  <a href="RoleList.php">Role Management</a>
                  <a href="PermissionsList.php">Permissions Mangement</a> 
                  <a href="RolePermissionList.php">Role Permissions Mangement</a>
                  <a href="UserRolesList.php">User-Role Assignment</a>
                  <a href="History.php">Login History</a>
                  <a href="Logout.php">Logout</a>
                
        </div> 

      <table style="padding-top:40px; padding-left: 200px" >
          <tr>
            <a href="UserRole.php"><button class="margin-button"><strong>Add New User-Role</strong></button></a> 
          </tr>
		<?php
        $query =  "SELECT ur.id as urId, ur.roleid as urRoleId, ur.userid as urUserId ,r.roleid as roleid, r.name as roleName, u.name as userId, u.name as userName FROM users u, roles r, user_role ur WHERE ur.roleid = r.roleid AND ur.userid = u.userid";
        $result = mysqli_query($conn, $query);
        $recordsFound = mysqli_num_rows($result);     
        
        if ($recordsFound > 0) {
          
          ?>
         
          <tr >

          <th class="tablestyle">ID</th>
          <th class="tablestyle">User</th>
          <th class="tablestyle">Role</th>
          <th class="tablestyle"> Edit</th>
          <th class="tablestyle">Delete</th>

          </tr>
            
        <?php
          while($row = mysqli_fetch_assoc($result)) {
                
            $id = $row['urId'];
            $role = $row['roleName'];
            $user = $row['userName']; 
            
            echo "<tr>";

            echo '<td style="padding-right: 50px">' . $id . "</td>";
            echo '<td style="padding-right: 50px">' . $user . "</td>";
            echo "<td>" . $role . "</td>";?>


            <th><a href="UserRole.php?id=<?php echo $id; ?>">Edit</a></th>
            <th><a href="UserRole.php?delete&id=<?php echo $id; ?>" onclick="return confirm('Are you really want to delete?')">Delete</a></th>
           <?php
            echo "</tr>";
            

            // echo "<tr> $id </tr>";
            // echo "<tr> $name</tr>";
            // echo "<tr> $email</tr>";

          }

          echo "</table>";
  }       
?>         
				

		
		</body>
</html>
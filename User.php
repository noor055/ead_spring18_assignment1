<?php
session_start();
require_once 'db.php';
$isNew = false;
$isEdit = false;
$isDelete = false;

if(isset($_GET['delete']) && isset($_GET['id'])) {
    $isDelete = true;
} else if(isset($_GET['id'])) {
    $isEdit = true;
} else {
    $isNew = true;
}
$current_record = false;
//

$msg = "";
$error = false;

if($isEdit) {
    $id = $_GET['id'];
    
    $query = "SELECT * FROM users WHERE userid = '$id'";
    $result = mysqli_query($conn, $query);
    $current_record = mysqli_fetch_assoc($result);
    //var_dump($current_record);
                
}
if($isDelete) {
    $id = $_GET['id'];
   // delete query
    $sql = "DELETE FROM users WHERE userid = '$id'";
    if (mysqli_query($conn, $sql)) {
        $error = false;
        $msg = "Record is deleted successfully.";
    }
    else {    
        $error = true;
        $msg = "Some Problem has occurred";
    }
}
if(isset($_POST["savebtn"])){

    $login = $_POST["logintxt"];
    $pass = $_POST["passtxt"];
    $name = $_POST["nametxt"];
    $email = $_POST["emailtxt"];
    $country = (isset($_POST["selectCountry"]) ? $_POST["selectCountry"] : '');
    $isadmin = (isset($_POST["isadmin"]) ? $_POST["isadmin"] : '');

    $sql = '';

    if($isNew) {
         $createdon = 'now()'; 
         $createdby = $_SESSION['user'];
         //var_dump($isNew);
         
         $sql = "SELECT login,email FROM users WHERE login = '$login' AND email = '$email' ";
         if(mysqli_num_rows(mysqli_query($conn, $sql))){
                $error = true;
                //var_dump($error);
                $msg = "User alreay exists.";
                
         }else{
             $sql = "INSERT INTO users (login,password,name,email,countryid, isadmin, createdon, createdby)
          VALUES ('$login', '$pass' , '$name' , '$email' , '$country', '$isadmin', $createdon, '$createdby') ";

          if (mysqli_query($conn, $sql) === TRUE) {
                $error = false;
                $msg = "Record is added successfully.";
            }
            else {
                $error = true;
                $msg = "Some Problem has occurred";
            }

         }         
    } else if($isEdit) {
           
            $id = $_GET['id'];
            // update query
          //  var_dump($isEdit);
            $sql = "SELECT login,email FROM users WHERE login = '$login' AND email = '$email' ";
             if(mysqli_num_rows(mysqli_query($conn, $sql))){
                    $error = true;
                    $msg = "User alreay exists.";
                    
            }else{
                    $sql = "UPDATE users 
                            SET login = '$login' , Password = '$pass' , name = '$name' , countryid = '$country' , isadmin = '$isadmin' WHERE userid = '$id'" ;
                            
                    if (mysqli_query($conn, $sql) === TRUE) {
                            $error = false;
                            $msg = "Record is updated successfully.";
                    }
                    else {
                            $error = true;
                            $msg = "Some Problem has occurred";
                    }
              }
                    
    }
}

?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>

	<body background = "grid.jpg" ">
		<div class="navbar">
                   <a href="Home.php">Home</a> 
                  <a href="UserList.php">User Management</a>
                  <a href="RoleList.php">Role Management</a>
                  <a href="PermissionsList.php">Permissions Mangement</a> 
                  <a href="RolePermissionList.php">Role Permissions Mangement</a>
                  <a href="UserRolesList.php">User-Role Assignment</a>
                  <a href="History.php">Login History</a>
                  <a href="Logout.php">Logout</a>
                
        </div>
         
         <div style="margin-bottom: 20px;margin-top: 20px">
             <a href="UserList.php"><button class="margin-button"><strong>View All Users</strong></button></a> 
       
         </div>
		<div style="background-color: white; position: absolute; right: 250px ; top: 100px ;left: 200px;margin-top: 50px">

                     <?php 
                        if(!empty($msg)) {
                            // form is submitted
                            if($error == true) {
                                // error
                                ?>
                                <div style="color:red">
                                    <?php  echo $msg; ?>
                                </div>
                                <?php
                            } else {
                                // no error
                                ?>
                                <div style="color:green">
                                    <?php  echo $msg; ?>
                                </div>
                                <?php
                            }
                        }

                     ?>
		 <?php if(!$isDelete) { ?>
                     <form action="User.php" method="POST">
        
                    <div style="background-color: black" display="inline-block" ; border: "thick" >
                        <h1 style="color: white ; width: 308px;padding:  10px 10px" ;><strong>User Management</strong></h1>
                    </div>

                    <div>

                        <input type="checkbox" name="isadmin" id="isadmin"
                        <?php echo ($current_record && $current_record['isadmin'] == '1' ? 'checked="true"' : ''); ?> />
                         <label><strong>isAdmin </strong></label>   

                    </div>

                    <div style="display: block;" >
                        <label ><strong>Login:</strong></label><br>
                        <div style="background-color: transparent; ">
                            <input  name="logintxt" type="text" id="login" 
                            value="<?php echo ($current_record ? $current_record['login'] : ''); ?>" required />
                        </div>
                    </div>
    

    
                    <div style="display: block;">
                        <label> <strong>Password:</strong></label><br>
                        <div style="background-color: transparent;">
                            <input  name="passtxt" type="Password" id="pass" required />
                        </div>
                    </div>


                    <div style="display: block;">
                        <label> <strong>Name:</strong></label><br>
                        <div style="background-color: transparent;">
                            <input  name="nametxt" type="text" id="name"
                             value="<?php echo ($current_record ? $current_record['name'] : ''); ?>" required />
                        </div>
                    </div>
 
                    <div style="display: block;">
                        <label> <strong>Email:</strong></label><br>
                        <div style="background-color: transparent;">
                            <input  name="emailtxt" type="text" id="email" value="<?php echo ($current_record ? $current_record['email'] : ''); ?>" required />
                        </div>
                    </div>

                    

                    <div style="display: block;">
                        <label> <strong>Country:</strong></label><br>
                        <div style="background-color: transparent;">
                            <select name="selectCountry"  id="country" required > 
                            <option selected disabled>--Select--</option>   
                            <?php 
                                $query = 'SELECT * FROM country ';
                                $result = mysqli_query($conn, $query);
                                while($row = mysqli_fetch_assoc($result)) {
                                   
                                    $selected = (isset($current_record) && $current_record['userid'] == $row['id'] ? 'selected' : '');

                                    echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['name'] . '</option>   ';
                                }
                                
                            ?>
                            </select>      
                        </div>
                    </div>
                    


                    <div style="background-color: black ; color: white ;padding: 10px 10px  ;margin: 8px 0px ;cursor: pointer; bottom:  50px; left: 50px ">
                            <input value="Save" type="submit" name="savebtn">
                            <input type="reset" value="Clear" />
                    </div>
                   

              </form>
              <?php } ?>
</div>
</body>
</html>

<?php

    session_start();
    require_once 'db.php';
  
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
 	

 	// check if user has ALREADY logged in
    if (isset($_SESSION['user'])) {
        header('Location: Home.php');
        exit;
    }

    $error = '';
    $msg = '';
    if (isset($_POST['txt_username']) && isset($_POST['txt_password'])) {

       	$query =  "SELECT * FROM users WHERE login = '" . $_POST['txt_username'] . "' AND password = '" . $_POST['txt_password'] . "'";

     
       	$result = mysqli_query($conn, $query);
       	if($row = mysqli_fetch_assoc($result)) {
       		// valid login
          $userid = $row['userid'];
          $login = $row['login'];
          $time = 'now()';
          $ip = get_client_ip();

          $sql = "INSERT INTO loginhistory(userid , login , logintime , machineip)
          VALUES ('$userid', '$login' , $time, '$ip' )";

    
            if (mysqli_query($conn, $sql) === TRUE) {
                $error = false;
                var_dump($error);
                $msg = "Record is added successfully in History.";
            }
            else {
                $error = true;
                $msg = "Some Problem has occurred";
            }

          $_SESSION['user'] = $row['userid'];
          header('Location: Home.php');

       	} else { 
                    $msg = 'Error: Invalid username or password.';
       	}
    }


?>

<html>
	<head>
		<title>Login</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>


        <body background="grid.jpg" >
		        
        
		<h1 style="text-align: center ; padding-top: 25px ; padding-right: 200px"> Security Manager</h1>

<br>
		<div style="background-color: white; position: absolute; right: 250px ; top: 100px ;left: 200px">
                    <?php 
					// if error variable is not empty display it
                    if ($error) { 
                    	echo $msg;
                	   }
                     ?>
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="loginForm">
							
						<div style="background-color: black" display="inline-block"  border: "thick" >
							<h1 style="color: white ; width: 308px;padding:  10px 10px" ><strong>Login User</strong></h1>
						</div>


				  		<div style="display: block;" >
	                        <label ><strong>Username:</strong></label><br>
	                        <div style="background-color: transparent; ">
                                    <input  name="txt_username" type="text" id="username" />
	                        </div>
	                    </div>
		

		
						<div style="display: block;">
	                        <label> <strong>Password:</strong></label><br>
	                        <div style="background-color: transparent;">
                                    <input  name="txt_password" type="Password" id="password" />
	                        </div>
	                    </div>
		
                         	
						<div style="background-color: black ; color: white ;padding: 10px 10px  ;margin: 8px 0px ;cursor: pointer; bottom:  50px; left: 50px ">
                            <input value="Login" type="submit" id="btnLogin">
                       
                    	</div>
					
        
					</form>
	</div>
				

		
	</body>
</html>
<?php
    session_start();
    require_once 'db.php';

    if(!isset($_SESSION['user'])){
    	header('Location: Login.php');
    	exit;
    }
?>

<html>
    <head>
		<link rel="stylesheet" type="text/css" href="style.css">
    </head>

<body background="grid.jpg">

	<div class="navbar">
                  <a href="Home.php">Home</a> 
                   <?php
                   		$isadmin = false;
                   		$query =  "SELECT isadmin FROM users WHERE userid = '" . $_SESSION['user'] . "'";

				       	$result = mysqli_query($conn, $query);

				       	// if record found in database
				       	if($row = mysqli_fetch_assoc($result)) {
			       			if($row['isadmin'] == '1') {
			       				$isadmin = true;
			       			}
				       	}
                  		if($isadmin){?>
                  
                  <a href="UserList.php">User Management</a>
                  <a href="RoleList.php">Role Management</a>
                  <a href="PermissionsList.php">Permissions Mangement</a> 
                  <a href="RolePermissionList.php">Role Permissions Mangement</a>
                  <a href="UserRolesList.php">User-Role Assignment</a>
                  <a href="History.php">Login History</a>

                  <?php }?>
                  <a href="Logout.php">Logout</a>
                
        </div>
        
    <div style="position: absolute; top: 50px;margin-bottom: 50px; margin-right: 50px">
       <h1>Welcome</h1>
        <?php
      //$user_role = getAllUserRoleByUserId($conn, $current_user['userid']);
      $query_userrole = "SELECT * FROM user_role WHERE userid = '" . $_SESSION['user'] . "'";
      $result_userrole = mysqli_query($conn, $query_userrole);
      $num_rows_userrole = mysqli_num_rows($result_userrole);
      if ($num_rows_userrole > 0) { 
          echo '<ol id="rolesList">';
          while ($ur = mysqli_fetch_assoc($result_userrole)) {
              echo '<li>';

              $query_roles = "SELECT * FROM roles WHERE roleid = '" . $ur['roleid'] . "'";
              $result_roles = mysqli_query($conn, $query_roles);
              if($result_roles && $role = mysqli_fetch_assoc($result_roles)) {
               echo '<strong>Role:</strong> ' . $role['name'] . '<br>';
              }
                  
             
              //$role_permission = getAllRolePermissionByRoleId($conn, $ur['roleid']);

              $query_rolepermission = "SELECT * FROM role_permission WHERE roleid = '" . $ur['roleid'] . "'";
              $result_rolepermission = mysqli_query($conn, $query_rolepermission);
              $num_rows_rolepermission = mysqli_num_rows($result_rolepermission);

              if ($num_rows_rolepermission > 0) {
                  echo '<strong>Permissions:</strong>';
                  echo '<ul>';
                  while ($rp = mysqli_fetch_assoc($result_rolepermission)) {
                      //$permission = getPermissionById($conn, $rp['permissionid']);
                    $query_permission = "SELECT * FROM permissions WHERE permissionid = '" . $rp['permissionid'] . "'";
                    $result_permission = mysqli_query($conn, $query_permission);

                    if($result_permission && $permission = mysqli_fetch_assoc($result_permission)) {
                      echo '<li>' . $permission['name'] . '</li>';
                    }
                  }
                  echo '</ul>';
              }
              echo '</li>';
          }
          echo '</ol>';
      }
      ?>    
    </div>
    <div>
            <button><a style="margin-top: 50px" href="Logout.php">Logout</a></button>

    </div>
  
  
       
   

</body>
</html>
 
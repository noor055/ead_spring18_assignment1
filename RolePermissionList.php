<?php
  session_start();
  require_once 'db.php';
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>Role-Permission List</title>		
	</head>

	<body background = "grid.jpg" >
        <div class="navbar">
                  <a href="Home.php">Home</a> 
                  <a href="UserList.php">User Management</a>
                  <a href="RoleList.php">Role Management</a>
                  <a href="PermissionList.php">Permissions Mangement</a> 
                  <a href="RolePermissionsList.php">Role Permissions Mangement</a>
                  <a href="UserRolesList.php">User-Role Assignment</a>
                  <a href="History.php">Login History</a>
                  <a href="Logout.php">Logout</a>
                
        </div>
  
  <table style="padding-top:40px; padding-left: 200px" >
    <tr style=" border: 1px solid black">
      <a href="RolePermissions.php"><button class="margin-button"><strong>Add New Role Permission</strong></button></a> 
    </tr>

	<?php

  $query = "SELECT rp.id as rpId, rp.roleid as rpRoleId, rp.permissionid as rpPermissionId ,r.roleid as roleid, r.name as roleName, p.permissionid as permissionId, p.name as permissionName FROM permissions p, roles r, role_permission rp WHERE rp.roleid = r.roleid AND rp.permissionid = p.permissionid";


  $result = mysqli_query($conn, $query);
  $recordsFound = mysqli_num_rows($result);     
  
  if ($recordsFound > 0) {
    
    ?>
   
    <tr style=" border: 1px solid black">

    <th class="tablestyle">ID</th>
    <th class="tablestyle">Role</th>
    <th class="tablestyle">Permission</th>
    <th class="tablestyle"> Edit</th>
    <th class="tablestyle">Delete</th>

    </tr>
      
  <?php
    while($row = mysqli_fetch_assoc($result)) {
          
      $id = $row['rpId'];
      $role = $row['roleName'];
      $permission = $row['permissionName']; 
      
      echo "<tr>";

      echo '<td style="padding-right: 50px">' . $id . "</td>";
      echo '<td style="padding-right: 50px">' . $role . "</td>";
      echo "<td>" . $permission . "</td>";?>


      <th><a href="RolePermissions.php?id=<?php echo $id; ?>">Edit</a></th>
      <th><a href="RolePermissions.php?delete&id=<?php echo $id; ?>" onclick="return confirm('Are you really want to delete?')">Delete</a></th>
     <?php
      echo "</tr>";
      

      // echo "<tr> $id </tr>";
      // echo "<tr> $name</tr>";
      // echo "<tr> $email</tr>";

    }

    echo "</table>";
  }       
?>
				

		
		</body>
</html>
<?php
session_start();
require_once 'db.php';

$isNew = false;
$isEdit = false;
$isDelete = false;

if(isset($_GET['delete']) && isset($_GET['id'])) {
    $isDelete = true;
} else if(isset($_GET['id'])) {
    $isEdit = true;
} else {
    $isNew = true;
}
$current_record = false;
//

$msg = "";
$error = false;

if($isEdit) {
    $id = $_GET['id'];
    //var_dump($_GET);
    // select
    $query = "SELECT * FROM permissions WHERE permissionid = '$id'";
    $result = mysqli_query($conn, $query);
    $current_record = mysqli_fetch_assoc($result);
    //var_dump($current_record);
                
}
if($isDelete) {
    $id = $_GET['id'];
   // delete query
    $sql = "DELETE FROM permissions WHERE permissionid = '$id'";
    if (mysqli_query($conn, $sql)) {
        $error = false;
        $msg = "Record is deleted successfully.";
    }
    else {    
        $error = true;
        $msg = "Some Problem has occurred";
    }
}
if(isset($_POST["savebtn"])){

    
    $name = $_POST["nametxt"];
    $description = $_POST["descritiontxt"];
    $sql = '';
    if($isNew) {

         $createdon = 'now()'; 
         $createdby = $_SESSION['user'];

         $sql = "SELECT name,description FROM permissions WHERE name = '$name' AND description = '$description' ";
          
          $result = mysqli_query($conn, $sql);
          if(mysqli_num_rows($result)){
             $error = true;
              $msg = "Role alreay exists.";
                    
            
          }else{
             $sql = "INSERT INTO permissions (name,description,createdon,createdby)
          VALUES ('$name' , '$description' , $createdon , '$createdby') ";

          if (mysqli_query($conn, $sql) === TRUE) {
                $error = false;
                $msg = "Record is added successfully.";
            }
            else {
                $error = true;
                $msg = "Some Problem has occurred";
            }

         }         
    } else if($isEdit) {
           
            $id = $_GET['id'];
            $sql = "SELECT name,description FROM permissions WHERE name = '$name' AND description = '$description' ";
             if(mysqli_num_rows(mysqli_query($conn, $sql))){
                    $error = true;
                    $msg = "Role alreay exists.";
                    
            }else{
                    $sql = "UPDATE permissions 
                            SET name = '$name' , description = '$description'";
                            
                    if (mysqli_query($conn, $sql) === TRUE) {
                            $error = false;
                            $msg = "Record is updated successfully.";
                    }
                    else {
                            $error = true;
                            $msg = "Some Problem has occurred";
                    }
              }
                    
    }
}

?>


<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>Permission Management</title>
	</head>

	<body background = "grid.jpg"  >
        <div class="navbar">
                <a href="Home.php">Home</a> 
                  <a href="UserList.php">User Management</a>
                  <a href="RoleList.php">Role Management</a>
                  <a href="PermissionsList.php">Permissions Mangement</a> 
                  <a href="RolePermissionList.php">Role Permissions Mangement</a>
                  <a href="UserRolesList.php">User-Role Assignment</a>
                  <a href="History.php">Login History</a>
                  <a href="Logout.php">Logout</a>
                
        </div>

         <div style="margin-bottom: 200px;margin-top: 20px">
             <a href="PermissionsList.php"><button class="margin-button"><strong>View All Permissions</strong></button></a> 
       
         </div>

		      <div style="background-color: white; position: absolute; right: 250px ; top: 100px ;left: 200px;  margin-top: 50px">
             <?php 
                        if(!empty($msg)) {
                            // form is submitted
                            if($error == true) {
                                // error
                                ?>
                                <div style="color:red">
                                    <?php  echo $msg; ?>
                                </div>
                                <?php
                            } else {
                                // no error
                                ?>
                                <div style="color:green">
                                    <?php  echo $msg; ?>
                                </div>
                                <?php
                            }
                        }

                     ?>
     <?php if(!$isDelete) { ?>
                     <form method="POST" action="permissions.php">
					<div style="background-color: black" display="inline-block" ; border: "thick" >
						<h1 style="color: white ; width: 308px;padding:  10px 10px" ;><strong>Permission Management</strong></h1>
					</div>
					
			  		<div style="display: block;" >
                        <label ><strong>Permission name:</strong></label><br>
                        <div style="background-color: transparent; ">
                            <input  name="nametxt" type="text" id="name"
                             value="<?php echo ($current_record ? $current_record['name'] : ''); ?>" required />  
                        </div>
                    </div>
	

	
					<div style="display: block;">
                        <label> <strong>Description:</strong></label><br>
                        <div style="background-color: transparent;">
                            <input  name="descritiontxt" type="text" id="descrition"
                             value="<?php echo ($current_record ? $current_record['description'] : ''); ?>" required />  
                        </div>
                    </div>
		
					
						<div style="background-color: black ; color: white ;padding: 10px 10px  ;margin: 8px 0px ;cursor: pointer; bottom:  50px; left: 50px ">
                            <input value="Save" type="submit" name="savebtn"/>
                            <input type="reset" value="Clear" />

            </div>
					
        
    

			 </form>
       <?php } ?>
       

    </div>          

		
		</body>
</html>
<?php
session_start();
require_once 'db.php';

if(!isset($_SESSION['user'])) {
  // redirect back to login
  header('Location: Login.php');
}

$isNew = false;
$isEdit = false;
$isDelete = false;

if(isset($_GET['delete']) && isset($_GET['id'])) {
    $isDelete = true;
} else if(isset($_GET['id'])) {
    $isEdit = true;
} else {
    $isNew = true;
}
$current_record = false;
//

$msg = "";
$error = false;

if($isEdit) {
    $id = $_GET['id'];
    //var_dump($_GET);
    // select
    $query = "Select * from role_permission where id='$id'";
    $result = mysqli_query($conn, $query);
    $current_record = mysqli_fetch_assoc($result);
    //var_dump($current_record);
                
}
if($isDelete) {
    $id = $_GET['id'];
   // delete query
    $sql = "DELETE FROM role_permission WHERE id = '$id'";
    if (mysqli_query($conn, $sql)) {
        $error = false;
        $msg = "Record is deleted successfully.";
    }
    else {    
        $error = true;
        $msg = "Some Problem has occurred";
    }
}
if(isset($_POST["savebtn"])){

    
    $role = $_POST["txtrole"];
    $permission = $_POST["txtpermission"];
    $sql = '';
    if($isNew) {
       
       $createdon = 'now()'; 
         $createdby = $_SESSION['user'];

         $sql = "SELECT * from role_permission where roleid='$role'";
         $result = mysqli_query($conn, $sql);

            if($result && mysqli_num_rows($result) > 0){
                $error = true;
                $msg = "Role && permission alreay exists.";
                
            
         }else{
             $sql = "INSERT INTO role_permission (roleid,permissionid)
					VALUES ('$role','$permission')";

          if (mysqli_query($conn, $sql) === TRUE) {
                $error = false;
                $msg = "Record is added successfully.";
            }
            else {
                $error = true;
                $msg = "Some Problem has occurred";
            }

    	}
    } else if($isEdit) {
           
            $id = $_GET['id'];
            $sql = "Select * from role_permission where roleid='$role'";

             if(mysqli_num_rows(mysqli_query($conn, $sql))){
                    $error = true;
                    $msg = "Role alreay exists.";
                    
            }else{
                    $sql = "UPDATE role_permission
			SET roleid='$role',permissionid='$permission'
			WHERE id='$id'";
                            
                    if (mysqli_query($conn, $sql) === TRUE) {
                            $error = false;
                            $msg = "Record is updated successfully.";
                    }
                    else {
                            $error = true;
                            $msg = "Some Problem has occurred";
                    }
              }
                    
    }
}

?>


<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		<title>Role-Permission Management</title>		
	</head>

	<body background = "grid.jpg" >
        <div class="navbar">
                  <a href="Home.php">Home</a> 
                  <a href="UserList.php">User Management</a>
                  <a href="RoleList.php">Role Management</a>
                  <a href="PermissionsList.php">Permissions Mangement</a> 
                  <a href="RolePermissionList.php">Role Permissions Mangement</a>
                  <a href="UserRolesList.php">User-Role Assignment</a>
                  <a href="History.php">Login History</a>
                  <a href="Logout.php">Logout</a>
                
        </div>
        
         <div style="margin-bottom: 200px;margin-top: 20px">
             <a href="RolePermissionList.php"><button class="margin-button"><strong>View All Role-Permissions</strong></button></a> 
       
         </div>

		<div style="background-color: white; position: absolute; right: 250px ; top: 100px ;left: 200px; margin-top: 50px">
          <?php 
                        if(!empty($msg)) {
                            // form is submitted
                            if($error == true) {
                                // error
                                ?>
                                <div style="color:red">
                                    <?php  echo $msg; ?>
                                </div>
                                <?php
                            } else {
                                // no error
                                ?>
                                <div style="color:green">
                                    <?php  echo $msg; ?>
                                </div>
                                <?php
                            }
                        }

                     ?>
     <?php if(!$isDelete) { ?>
                     <form action="RolePermissions.php" method="POST">
					<div style="background-color: black" display="inline-block" ; border: "thick" >
						<h1 style="color: white ; width: 308px;padding:  10px 10px" ;><strong>Role Permission Management</strong></h1>
					</div>

				

			  		<div style="display: block; padding-left: 20px" >
                        <label ><strong>Role :</strong></label><br>
                        <div style="background-color: transparent">
                            <select  name="txtrole" id="role" required>
                             <option selected disabled>--Select--</option>   
                            <?php 
                                $query = 'SELECT * FROM roles ';
                                $result = mysqli_query($conn, $query);
                                while($row = mysqli_fetch_assoc($result)) {
                                    $selected = (isset($current_record) && $current_record['roleid'] == $row['roleid'] ? 'selected' : '');

                                    echo '<option value="' . $row['roleid'] . '" ' . $selected . '>' . $row['name'] . '</option>   ';
                                }
                                
                            ?>  </select> 
                         </div>
                    </div>
	

	
					<div style="display: block; padding-left: 20px">
                        <label> <strong>Permission:</strong></label><br>
                        <div style="background-color: transparent;">
                        <select name="txtpermission" id="permission" required>
                               <option selected disabled>--Select--</option>   
                            <?php 
                                $query = 'SELECT * FROM permissions ';
                                $result = mysqli_query($conn, $query);
                                while($row = mysqli_fetch_assoc($result)) {
                                   
                                    $selected = (isset($current_record) && $current_record['permissionid'] == $row['permissionid'] ? 'selected' : '');

                                    echo '<option value="' . $row['permissionid'] . '" ' . $selected . '>' . $row['name'] . '</option>   ';
                                }
                                
                            ?> 
                            </select>  
                        </div>
                    </div>
		
					
						<div style="background-color: black ; color: white ;padding: 10px 10px  ;margin: 8px 0px ;cursor: pointer; bottom:  50px; left: 50px ;padding-left: 75px">
                           <input value="Save" type="submit" name="savebtn"/>
                            <input type="reset" value="Clear" />

                    	</div>
					
        
   				  </form>
          <?php } ?>
    </div>          

				

		
		</body>
</html>
<?php
session_start();
require_once 'db.php';

if(!isset($_SESSION['user'])) {
  // redirect back to login
  header('Location: Login.php');
}

$isNew = false;
$isEdit = false;
$isDelete = false;

if(isset($_GET['delete']) && isset($_GET['id'])) {
    $isDelete = true;
} else if(isset($_GET['id'])) {
    $isEdit = true;
} else {
    $isNew = true;
}
$current_record = false;
//

$msg = "";
$error = false;

if($isEdit) {
    $id = $_GET['id'];
    //var_dump($_GET);
    // select
    $query = "SELECT * from user_role where id='$id'";
    $result = mysqli_query($conn, $query);
    $current_record = mysqli_fetch_assoc($result);
    //var_dump($current_record);
                
}
if($isDelete) {
    $id = $_GET['id'];
   // delete query
    $sql = "DELETE FROM user_role WHERE id = '$id'";
    if (mysqli_query($conn, $sql)) {
        $error = false;
        $msg = "Record is deleted successfully.";
    }
    else {    
        $error = true;
        $msg = "Some Problem has occurred";
    }
}
if(isset($_POST["savebtn"])){

    
    $role = $_POST["txtRole"];
    $user = $_POST["txtUser"];
    $sql = '';
    if($isNew) {
       // var_dump($isNew);
       // exit();
       $createdon = 'now()'; 
         $createdby = $_SESSION['user'];

         $sql = "SELECT * from user_role where roleid='$role' AND usrid = '$user'";
         $result = mysqli_query($conn, $sql);

            if($result && mysqli_num_rows($result) > 0){
                $error = true;
                $msg = "Role && user alreay exists.";
                
            
         }else{
             $sql = "INSERT INTO user_role (userid,roleid)
          VALUES ('$user','$role')";
         // var_dump($sql);


          if (mysqli_query($conn, $sql) === TRUE) {
                $error = false;
                $msg = "Record is added successfully.";
            }
            else {
                $error = true;
                $msg = "Some Problem has occurred";
            }

      }
    } else if($isEdit) {
           
            $id = $_GET['id'];
            $sql = "SELECT * from user_role where roleid='$role'";

             if(mysqli_num_rows(mysqli_query($conn, $sql))){
                    $error = true;
                    $msg = "Role alreay exists.";
                    
            }else{
                    $sql = "UPDATE user_role
      SET userid='$user',roleid='$role'
      WHERE id='$id'";
                            
                    if (mysqli_query($conn, $sql) === TRUE) {
                            $error = false;
                            $msg = "Record is updated successfully.";
                    }
                    else {
                            $error = true;
                            $msg = "Some Problem has occurred";
                    }
              }
                    
    }
}

?>

<html>
	<head>
		<link rel="stylesheet" type="text/css" href="style.css">
		 <title>User-Role </title>



	</head>

	<body background = "grid.jpg" >
           <div class="navbar">
                   <a href="Home.php">Home</a> 
                  <a href="UserList.php">User Management</a>
                  <a href="RoleList.php">Role Management</a>
                  <a href="PermissionsList.php">Permissions Mangement</a> 
                  <a href="RolePermissionList.php">Role Permissions Mangement</a>
                  <a href="UserRolesList.php">User-Role Assignment</a>
                  <a href="History.php">Login History</a>
                  <a href="Logout.php">Logout</a>
                
        </div> 

		 <div style="margin-bottom: 20px;margin-top: 20px">
             <a href="UserRolesList.php"><button class="margin-button"><strong>View All User-Role</strong></button></a> 
       
         </div>
        <div style="background-color: white; position: absolute; right: 250px ; top: 100px ;left: 200px;margin-top: 50px">
        
         <?php 
                        if(!empty($msg)) {
                            // form is submitted
                            if($error == true) {
                                // error
                                ?>
                                <div style="color:red">
                                    <?php  echo $msg; ?>
                                </div>
                                <?php
                            } else {
                                // no error
                                ?>
                                <div style="color:green">
                                    <?php  echo $msg; ?>
                                </div>
                                <?php
                            }
                        }

                     ?>
     <?php if(!$isDelete) { ?>
                    
            <form action="UserRole.php" method="POST">
          <div style="background-color: black" display="inline-block" ; border: "thick" >
            <h1 style="color: white ; width: 308px;padding:  10px 10px" ;><strong>User Role </strong></h1>
          </div>
          


            <div style="display: block;" >
                        <label ><strong>Role :</strong></label><br>
                        <div style="background-color: transparent; ">
                            <select  name="txtRole" id="role" required>
                               <option selected disabled>--Select--</option>   
                            <?php 
                                $query = 'SELECT * FROM roles ';
                                $result = mysqli_query($conn, $query);
                                while($row = mysqli_fetch_assoc($result)) {
                                    $selected = (isset($current_record) && $current_record['roleid'] == $row['roleid'] ? 'selected' : '');

                                    echo '<option value="' . $row['roleid'] . '" ' . $selected . '>' . $row['name'] . '</option>   ';
                                }
                                
                            ?>  </select> 
                        
                        </div>
            </div>
  

  
          <div style="display: block;">
                        <label> <strong>User:</strong></label><br>
                        <div style="background-color: transparent;">
                            <select  name="txtUser" id="user" required>
                              <option selected disabled>--Select--</option>   
                            <?php 
                                $query = 'SELECT * FROM users ';
                                $result = mysqli_query($conn, $query);
                                while($row = mysqli_fetch_assoc($result)) {
                                    $selected = (isset($current_record) && $current_record['userid'] == $row['userid'] ? 'selected' : '');

                                    echo '<option value="' . $row['userid'] . '" ' . $selected . '>' . $row['name'] . '</option>   ';
                                }
                                
                            ?> 
                            </select>
                        </div>
                    </div>
    
          
            <div style="background-color: black ; color: white ;padding: 10px 10px  ;margin: 8px 0px ;cursor: pointer; bottom:  50px; left: 50px ">
                              <input value="Save" type="submit" name="savebtn"/>
                            <input type="reset" value="Clear" />
                      </div>
          
        
    
  </form>  
         
				

		<?php } ?>
		</body>
</html>